import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class DomHandler {

    // 按照左儿子，右兄弟的顺序遍历整棵节点树
    static void handleChildAttr(Node node) {
        if (node.hasChildNodes()) {
            // 有儿子递归
            Node left = node.getFirstChild();
            handleChildAttr(left);
            Node next = left.getNextSibling();
            // 左儿子有兄弟递归
            while (next != null) {
                handleChildAttr(next);
                next = next.getNextSibling();
            }
        }
        // 没儿子了，开始处理
        if (!node.hasAttributes()) {
            // 没有attr不做更改
            return;
        }
        // 有attr则把attr变成一个元素
        NamedNodeMap namedNodeMap = node.getAttributes();
        Node attr, temp, text = null;
        Document document = node.getOwnerDocument();
        int len = namedNodeMap.getLength();
        for (int index = 0; index < len; index++) {
            // 因为后面会删除第一个属性，所以每次都是第一个
            attr = namedNodeMap.item(0);
            temp = document.createElement(attr.getNodeName());
            text = document.createTextNode(attr.getNodeValue());
            temp.appendChild(text);
            node.appendChild(temp);
            namedNodeMap.removeNamedItem(attr.getNodeName());
        }
    }
}
