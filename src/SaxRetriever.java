import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

public class SaxRetriever extends DefaultHandler {

    public void loadByName(String uri) {
        DefaultHandler saxHandler = this;
        SAXParserFactory saxFactory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = saxFactory.newSAXParser();
            saxParser.parse(new File(uri), saxHandler);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public void startDocument() {
        System.out.println("ipo loading...");
    }

    @Override
    public void startElement(String uri, String localName, String qualifiedName, Attributes attributes) {
//        System.out.println(qualifiedName + "-+-" + attributes.getQName(0));
        int attrLen = attributes.getLength();
        if (attrLen <= 0) {
            return;
        }
        for (int index = 0; index < attrLen; index++) {
            System.out.println(attributes.getQName(index)+":"+attributes.getValue(index));
        }
    }

    @Override
    public void characters(char characters[], int start, int length) {
//        System.out.println("characters: \"" + new String(characters, start, length) + "\"");
    }

    @Override
    public void endElement(String uri, String localName, String qualifiedName) {
    }
}
