import org.w3c.dom.Document;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Main {
    private static String fileName = "ipo.xml";
    private static String[] names = {"ABC", "IBM"};

    public static void main(String[] args) {
//        System.out.println("Hello World!");
        saxRetrieve();
//        dom4jDivide();
//        domDivide();
        domDivideOnce();
    }

    // 使用sax解析器，输出属性到控制台
    private static void saxRetrieve() {
        SaxRetriever saxRetriever = new SaxRetriever();
        saxRetriever.loadByName(fileName);
    }

    // 使用dom分离ipo.xml到两个文件（对读取到内存中的模型扫描1次,空间占用增加n倍,n取决于company的数量）
    private static void domDivideOnce() {
        DomDivider domDivider = new DomDivider();
        domDivider.loadByName(fileName);
        HashMap<String, Document> hashMap = domDivider.handleAllOnce(names);
        Iterator<Map.Entry<String, Document>> entries = hashMap.entrySet().iterator();

        while (entries.hasNext()) {
            Map.Entry<String, Document> entry = entries.next();
            try (FileOutputStream fileOutputStream = new FileOutputStream(entry.getKey() + "_COMP.xml")) {
                DomWriter.printToStream(entry.getValue(), fileOutputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        for (Document document : hashMap.values()) {
//            DomWriter.printToStream(document, System.out);
//        }
    }

    // 使用dom分离ipo.xml到两个文件（对读取到内存中的模型扫描n次,n取决于company的数量）
    private static void domDivide() {
        DomDivider domDivider = new DomDivider();
        domDivider.loadByName(fileName);
//        FileOutputStream fileOutputStream = null;
        for (String name : names)
            // JDK 7 以上 try-with-resource
            try (FileOutputStream fileOutputStream = new FileOutputStream(name + "_COMP.xml")) {
                DomWriter.printToStream(domDivider.handleDomOfCompany(name), fileOutputStream);
//                divider.printToStream(divider.handleDomOfCompany(name), System.out);
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    // 使用dom4j分离ipo.xml到两个文件
    private static void dom4jDivide() {
        Dom4jDivider dom4jDivider = new Dom4jDivider();
        dom4jDivider.loadByName(fileName);
        for (String name : names) {
            dom4jDivider.divideOrderByName(name);
        }
    }
}