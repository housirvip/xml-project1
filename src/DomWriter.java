import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.OutputStream;

public class DomWriter {
    //将node的XML字符串输出到流
    public static void printToStream(Object o, OutputStream outputStream) {
        Node node = null;
        if (o instanceof Document) {
            node = ((Document) o).getDocumentElement();
        } else if (o instanceof Node) {
            node = (Node) o;
        } else {
            System.out.println("错误的打印类型，仅支持xml节点类型");
        }
        TransformerFactory transFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transFactory.newTransformer();
            transformer.setOutputProperty("encoding", "utf-8");
            transformer.setOutputProperty("indent", "no");

            DOMSource source = new DOMSource();
            source.setNode(node);
            StreamResult result = new StreamResult();
            result.setOutputStream(outputStream);
            transformer.transform(source, result);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}
