import org.dom4j.*;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class Dom4jDivider {
    private Document document;
    private Element root;

    public void loadByName(String name) {
        SAXReader reader = new SAXReader();
        try {
            document = reader.read(new File(name));
            root = document.getRootElement();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void handleChildAttr(Element element) {
        // 有子元素则对所有子元素递归
        for (Iterator<Element> iterator = element.elementIterator(); iterator.hasNext(); ) {
            handleChildAttr(iterator.next());
        }
        // 对本身attr进行处理
        Attribute attribute;
        Element temp;
        Iterator<Attribute> iterator = element.attributeIterator();
        while (iterator.hasNext()) {
            attribute = iterator.next();
            element.remove(attribute);
            if (attribute.getName().equals("comp_name"))
                return;
            temp = element.addElement(attribute.getName());
            temp.setText(attribute.getValue());
            // 删除过attr后需要再次赋值迭代器
            iterator = element.attributeIterator();
        }
        // 没有就算了
    }

    public void divideOrderByName(String name) {
        String fileName = name + "_COMP";
        List<Element> orders = root.elements("purchaseOrder");
        Element order;
        Document newDom = DocumentHelper.createDocument();
        Element temp = newDom.addElement("purchaseOrders");
        temp = temp.addElement(fileName);
        for (Iterator<Element> iterator = orders.iterator(); iterator.hasNext(); ) {
            order = iterator.next();
            order = order.createCopy();
            if (order.attributeValue("comp_name").equals(name)) {
                temp.add(order);
                handleChildAttr(order);
            }
        }
        try (FileWriter fileWriter = new FileWriter(fileName + ".xml")) {
            XMLWriter writer = new XMLWriter(fileWriter);
            writer.write(newDom);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
