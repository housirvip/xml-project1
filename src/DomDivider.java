import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.util.HashMap;

public class DomDivider {
    private Document document;
    private DocumentBuilderFactory factory;
    private DocumentBuilder builder;

    public void loadByName(String fileName) {
        try {
            this.factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();
            this.document = builder.parse(fileName);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    // 一次读取把所有的xml节点处理完成，以空间换时间
    public HashMap<String, Document> handleAllOnce(String[] names) {
        HashMap<String, Document> hashMap = new HashMap<>();
        Document xmlDoc;
        Node orders, company, order;
        for (String name : names) {
            xmlDoc = builder.newDocument();
            hashMap.put(name, xmlDoc);
            orders = xmlDoc.createElement("purchaseOrders");
            company = xmlDoc.createElement(name + "_COMP");
            orders.appendChild(company);
            xmlDoc.appendChild(orders);
        }
        NodeList nodeList = selectNodes("/purchaseOrders/purchaseOrder", this.document.getDocumentElement());
        for (int index = 0; index < nodeList.getLength(); index++) {
            order = nodeList.item(index);
            // 获取第一个attr即可
            String name = order.getAttributes().item(0).getNodeValue();
            xmlDoc = hashMap.get(name);
            company = xmlDoc.getFirstChild().getFirstChild();
            order.getAttributes().removeNamedItem("comp_name");
            DomHandler.handleChildAttr(order);
            company.appendChild(xmlDoc.importNode(order, true));
        }
        return hashMap;
    }

    // 针对公司名称进行分类
    public Document handleDomOfCompany(String name) {
        Document xmlDoc;
        xmlDoc = builder.newDocument();
        Node orders = xmlDoc.createElement("purchaseOrders");
        NodeList nodeList = selectNodes("/purchaseOrders/purchaseOrder[@comp_name='" + name + "']", this.document.getDocumentElement());
//        NodeList nodeList = this.document.getElementsByTagName("purchaseOrder");
        Node company = xmlDoc.createElement(name + "_COMP");
        Node node = null;
        for (int index = 0; index < nodeList.getLength(); index++) {
            node = nodeList.item(index);
            node.getAttributes().removeNamedItem("comp_name");
            DomHandler.handleChildAttr(node);
            company.appendChild(xmlDoc.importNode(node, true));
        }
        orders.appendChild(company);
        xmlDoc.appendChild(orders);
        return xmlDoc;
    }

    // 查找节点，返回符合条件的节点集
    public NodeList selectNodes(String express, Object source) {
        NodeList result = null;
        XPathFactory xpathFactory = XPathFactory.newInstance();
        XPath xpath = xpathFactory.newXPath();
        try {
            result = (NodeList) xpath.evaluate(express, source, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        return result;
    }

}
